package main

import (
	"fmt"
	"os"
)

func main() {
	_, exists := os.LookupEnv("LOOP")

	if exists {
		fmt.Println("Launch and Loop")
		for {
		}
	} else {
		fmt.Println("Launch and Quit")
	}
}
