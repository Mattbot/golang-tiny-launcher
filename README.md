# golang-tiny-launcher

Tiny Launcher is a Golang binary meant to be run in a docker container as a part of docker-compose project. 
Services meant to run in a group can be launched by the 'depends_on:' directive in Tiny Launcher, service definition. 
Modes: quit or loop.

```docker-compose up --attach-dependencies loop```