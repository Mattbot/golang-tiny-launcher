FROM scratch

WORKDIR /

COPY ./golang-tiny-launcher /

ENTRYPOINT ["/golang-tiny-launcher"]

CMD ''